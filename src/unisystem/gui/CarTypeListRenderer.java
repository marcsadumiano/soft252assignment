/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unisystem.gui;

import java.awt.Color;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import unisystem.model.Car;

/**
 *
 * @author Marc Gil Sadumiano
 */
public class CarTypeListRenderer extends DefaultListCellRenderer implements ListCellRenderer<Object>{

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        Car car = (Car)value;
        setText(car.getMake() + " " + car.getModel() + " " + car.getYear());
        setEnabled(true);
        setBackground(Color.white);
        return this;
    }
    
    
}
