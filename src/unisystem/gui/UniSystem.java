/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unisystem.gui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import unisystem.model.*;
import unisystem.observer.*;

/**
 *
 * @author Marc Gil Sadumiano...
 * SOFT252 Assignment - Car Rental Booking
 */
public class UniSystem extends JFrame implements IObserver {
    
    private final UniSystemWrapper wrapper = new UniSystemWrapper();
    
    private Staff staffCurrent;
    private Car carCurrent;
    private CarAllocation carAllocCurrent;
    private CarAllocationList carAllocList;
    private ArrayList<Car> cars;
    private ArrayList<Car> allocatedCars;
    private ArrayList<Staff> staffs;
    
    private DefaultListModel modelAllCars;
    private DefaultListModel modelAllocatedCars;
    private DefaultListModel modelCarType;
    private DefaultListModel modelAllocations;
    private DefaultListModel modelStaffs;
    private JList lstAllCars;
    private JList lstAllocatedCars;
    private JList lstCarType;
    private JList staffList;
    
    //variable declarations for gui components
    //<editor-fold>
    private Container container = getContentPane();
    private CardLayout cardLayout = new CardLayout();
    
    private JPanel jpMenu;
    private JButton btnCars;
    private JButton btnCreateAllocation;
    private JButton btnAllocatedCars;
    private JButton btnCreateStaff;
    private JButton btnStaff;
    
    private JPanel jpCars;
    private JLabel lblMake; 
    private JLabel lblModel; 
    private JLabel lblYear; 
    private JLabel lblRegistration; 
    private JLabel lblDoors; 
    private JLabel lblNextService; 
    private JLabel lblClassification;
    private JLabel lblState;
    private DefaultListModel modelServices;
    private JButton btnEdit;
    private JButton btnAddService;
    
    private JPanel jpAcBody;
    private JLabel lblDueAC;
    private JLabel lblIdAC;
    private JLabel lblNameAC;
    private JLabel lblCarAllocationAC;
    private JLabel lblMakeAC;
    private JLabel lblModelAC;
    private JLabel lblYearAC;
    private JButton btnEditAC;
    private JButton btnEditStaff;
    
    private JPanel jpAllocatedCars;
    private JLabel lblNameCCA;
    private JLabel lblDobCCA;
    private JLabel lblCarTypeCCA;
    private JLabel lblRegistrationCCA;
    private JLabel lblMakeCCA;
    private JLabel lblModelCCA;
    private JLabel lblYearCCA;
    private JComboBox cbxLoan;
    private DefaultComboBoxModel<String> modelLoan;
    private JButton btnSubmit;
    
    private JMenuBar menuBar;
    
    //</editor-fold>
    
    
    public UniSystem(){
        super("UniSystem");
        staffs = new ArrayList<>();
        cars = new ArrayList<>();
        allocatedCars = new ArrayList<>();
        carAllocList = new CarAllocationList();
        modelAllCars = new DefaultListModel();
        modelAllocatedCars = new DefaultListModel();
        modelCarType = new DefaultListModel();
        modelAllocations = new DefaultListModel();
        modelStaffs = new DefaultListModel();
        
        init();
        refreshList();
        
    }
    
    public static void main(String[] args){
        new UniSystem();
    }
    
    private void init(){
        container.setLayout(cardLayout);
        lstAllCars = new JList(modelAllCars);
        
        //lstAllocatedCars = new JList(modelAllocatedCars);
        //lstAllocatedCars = new JList(modelAllocations);
        //lstAllocatedCars.setCellRenderer(new AllocationListRenderer());

        lstAllocatedCars = new JList();
        lstAllocatedCars.setCellRenderer(new AllocationListRenderer());
        lstAllocatedCars.setModel(modelAllocations);
        
        lstCarType = new JList(modelCarType); 
        lstCarType.setCellRenderer(new CarTypeListRenderer());
        
        staffList = new JList(modelStaffs);
        
        //Main Menu
        //<editor-fold defaultstate="collapsed" desc="Main Menu Initialisation">
        jpMenu = new JPanel(null);
        container.add(jpMenu, "main menu");
        btnCars = new JButton("Cars");
        btnCreateAllocation = new JButton("Create Allocation");
        btnAllocatedCars = new JButton("Allocated Cars");
        //btnCreateStaff = new JButton("Create Staff");
        btnStaff = new JButton("Staffs");
        jpMenu.add(btnCars);
        jpMenu.add(btnCreateAllocation);
        jpMenu.add(btnAllocatedCars);
        //jpMenu.add(btnCreateStaff);
        jpMenu.add(btnStaff);
        
        //to align horizantally: frame's width / 2 - (component width / 2)
        
        btnCars.setBounds(375, 266, 150, 50);
        btnCreateAllocation.setBounds(375, 326, 150, 50);
        btnAllocatedCars.setBounds(375, 386, 150, 50);
        //btnCreateStaff.setBounds(375, 446, 150, 50);
        btnStaff.setBounds(375, 446, 150, 50);
        
        btnCars.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(container, "car panel");
                setTitle("UniSystem - Cars");
            }
        });
        
        btnAllocatedCars.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(container, "allocated panel");
                setTitle("UniSystem - Allocations");
            }
        });
        
        btnCreateAllocation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //avoid un-wanted car allocation (might be from previous selection)
                carCurrent = null;
                staffCurrent = null;
                cardLayout.show(container, "create panel");
                setTitle("UniSystem - Create Allocation");
            }
        });
        
//        btnCreateStaff.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                CreateStaff createStaffDialog = new CreateStaff(null, true);
//                createStaffDialog.setParent(UniSystem.this);
//                createStaffDialog.setVisible(true);
//            }
//        });
          btnStaff.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setTitle("Staffs");
                cardLayout.show(container, "staffs");
            }
        });
        
        //</editor-fold>
        
        //Cars
        //<editor-fold defaultstate="collapsed" desc="Cars Initialisation">
        jpCars = new JPanel(new BorderLayout(50, 10));
        
        JPanel jpGrid = new JPanel(new GridLayout(0,1));
        JPanel jpBack = new JPanel(new BorderLayout());
        JButton btnBack = new JButton("< Back");
        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.previous(container);
                setTitle("UniSystem ");
            }
        });
        jpBack.setBorder(new EmptyBorder(10, 10, 10, 10));
        jpBack.add(btnBack, BorderLayout.WEST);
        jpGrid.add(jpBack);
        JPanel jpListLabel = new JPanel(new BorderLayout());
        jpListLabel.setBorder(new EmptyBorder(0, 50, 0, 0));
        JLabel lblCarList = new JLabel("Cars:");
        jpListLabel.add(lblCarList);
        jpGrid.add(jpListLabel);
        
        jpCars.add(jpGrid, BorderLayout.NORTH);
        container.add(jpCars,"car panel");
        
        lstAllCars.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt){
                int index = lstAllCars.getSelectedIndex();
                if(cars != null){
                    if(index != -1){ //prevent from throwing arrayindexoutofbounds
                        carCurrent = cars.get(index);
                        refreshList();
                        lblMake.setText(carCurrent.getMake());
                        lblModel.setText(carCurrent.getModel());
                        lblYear.setText(String.valueOf(carCurrent.getYear()));
                        lblDoors.setText(String.valueOf(carCurrent.getNoOFDoors()));
                        lblRegistration.setText(carCurrent.getRegisterNumber());
                        lblClassification.setText(carCurrent.getClassification().toString());
                        lblState.setText(carCurrent.getState().toString());
                        lblNextService.setText(carCurrent.getNextService().toString());
                        btnEdit.setVisible(true);
                        btnAddService.setVisible(true);
                    }
                }
                  
            }
        });
        
        JScrollPane jspScroll = new JScrollPane(lstAllCars);
        jspScroll.setBorder(new EmptyBorder(0, 20, 20, 0));
        
        JPanel jpCarsBody = new JPanel(new GridLayout(1, 3));
        jpCarsBody.add(jspScroll);
        jpCars.add(jpCarsBody, BorderLayout.CENTER);
        
        //Cars form area
        JPanel jpForm = new JPanel(new GridBagLayout());
        jpCarsBody.add(jpForm);
        GridBagConstraints c = new GridBagConstraints();
        c.weighty = 1;
        c.weightx = 1;
        //--1st column
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.LINE_END;
        //c.insets = new Insets(0, 0, 0, 100);
        c.ipadx = 100;
        jpForm.add(new JLabel("Make:"), c);
        c.gridy++;
        jpForm.add(new JLabel("Model:"), c);
        c.gridy++;
        jpForm.add(new JLabel("Year:"), c);
        c.gridy++;
        jpForm.add(new JLabel("Registration No.:"), c);
        c.gridy++;
        jpForm.add(new JLabel("No. of doors:"), c);
        c.gridy++;
        jpForm.add(new JLabel("Next Service:"), c);
        c.gridy++;
        jpForm.add(new JLabel("Classification:"), c);
        c.gridy++;
        jpForm.add(new JLabel("Car's State:"), c);
        c.gridy++;
        c.anchor = GridBagConstraints.LINE_END;
        c.ipadx = 0;
        btnEdit = new JButton("Edit");
        btnEdit.setVisible(false);
        btnEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EditCar editCarDialog = new EditCar(null, true);
                editCarDialog.setTitle("UniSystem - Edit Car Details");
                editCarDialog.setParent(UniSystem.this);
                if(carCurrent != null){
                    editCarDialog.setCar(carCurrent);
                }
                editCarDialog.setVisible(true);
            }
        });
        jpForm.add(btnEdit, c);
        
        //--2nd column
        c.gridx = 1;
        c.gridy = 0;
        c.anchor = GridBagConstraints.LINE_START; 
        lblMake = new JLabel("");
        lblModel = new JLabel("");
        lblYear = new JLabel("");
        lblRegistration = new JLabel("");
        lblDoors = new JLabel("");
        lblNextService = new JLabel("");
        lblClassification = new JLabel("");
        lblState = new JLabel("");
        
        jpForm.add(lblMake, c);
        c.gridy++;
        jpForm.add(lblModel, c);
        c.gridy++;
        jpForm.add(lblYear, c);
        c.gridy++;
        jpForm.add(lblRegistration, c);
        c.gridy++;
        jpForm.add(lblDoors, c);
        c.gridy++;
        jpForm.add(lblNextService, c);
        c.gridy++;
        jpForm.add(lblClassification, c);
        c.gridy++;
        jpForm.add(lblState, c);
        c.gridy++;
        c.insets = new Insets(0, 5, 0, 0);
        JButton btnCreate = new JButton("New Car");
        btnCreate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CreateCar createCarDialog = new CreateCar(null, true);
                createCarDialog.setTitle("UniSystem - Create Car");
                createCarDialog.setParent(UniSystem.this);
                createCarDialog.setVisible(true);
                
            }
        });
        jpForm.add(btnCreate, c);
        
        //car body gridlayout 3rd col
        modelServices = new DefaultListModel();
        JList lstCarServices = new JList(modelServices);
        JScrollPane jspCarServices = new JScrollPane(lstCarServices);
        jspCarServices.setBorder(new EmptyBorder(0, 0, 20, 20));
        jpCarsBody.add(jspCarServices);
        c.gridy++;
        btnAddService = new JButton("+ Service");
        btnAddService.setVisible(false);
        btnAddService.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddServiceDate asd = new AddServiceDate(null, true);
                if(carCurrent != null){
                    asd.setCar(carCurrent);
                }
                asd.setVisible(true);
            }
        });
        jpForm.add(btnAddService, c);
        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        //</editor-fold>
        
        // Allocated Cars (AC)
        //<editor-fold defaultstate="collapsed" desc="Cars Allocated Panel Initialisation">
         
        
        jpAcBody = new JPanel(new BorderLayout(5, 1));
        container.add(jpAcBody, "allocated panel");
        
        //AC body header
        JPanel jpAcHeaderCont = new JPanel(new GridLayout(0, 1));
        JPanel jpAcHeader1 = new JPanel(new BorderLayout());
        jpAcHeaderCont.add(jpAcHeader1);
        jpAcBody.add(jpAcHeaderCont, BorderLayout.NORTH);
        JButton btnCarBack = new JButton("< Back");
        jpAcHeader1.setBorder(new EmptyBorder(10, 10, 10, 10));
        btnCarBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.first(container);
            }
        });
        jpAcHeader1.add(btnCarBack, BorderLayout.WEST);
        JPanel jpAcHeader2 = new JPanel(new BorderLayout());
        jpAcHeader2.setBorder(new EmptyBorder(0, 50, 5, 0));
        jpAcHeader2.add(new JLabel("Cars on loan:"), BorderLayout.WEST);
        jpAcHeaderCont.add(jpAcHeader2);
        
        //AC body content
        JPanel jpAcContent = new JPanel(new GridLayout(1, 2));
        jpAcBody.add(jpAcContent, BorderLayout.CENTER);
        
        //-----------------cars on loan list, 1st column of this borderlayout ^
        JScrollPane jspAC = new JScrollPane();
        jspAC.setBorder(new EmptyBorder(0, 20, 20, 0));
        
        //jspAC.setViewportView(lstAllocatedCars);
        jspAC.setViewportView(lstAllocatedCars);
        lstAllocatedCars.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt){
                int index = lstAllocatedCars.getSelectedIndex();
//                if(allocatedCars != null){
//                    if(index != -1){ //prevent from throwing arrayindexoutofbounds
//                        
//                        carCurrent = allocatedCars.get(index);
//                        lblMakeAC.setText(carCurrent.getMake());
//                        lblModelAC.setText(carCurrent.getModel());
//                        lblYearAC.setText(String.valueOf(carCurrent.getYear()));
//                        btnEditAC.setVisible(true);
//                        btnEditStaff.setVisible(true);
//                    }
//                }
                CarAllocation tempAlloc = (CarAllocation) lstAllocatedCars.getModel().getElementAt(index);
                carCurrent = tempAlloc.getCar();
                staffCurrent = tempAlloc.getStaff();
                lblMakeAC.setText(carCurrent.getMake());
                lblModelAC.setText(carCurrent.getModel());
                lblYearAC.setText(String.valueOf(carCurrent.getYear()));
                lblDueAC.setText(LocalDate.now().plusDays(tempAlloc.getDuration().toDays()).toString());
                lblIdAC.setText(staffCurrent.getStaffId());
                lblNameAC.setText(staffCurrent.getName());
                lblCarAllocationAC.setText(staffCurrent.getCarType().toString());
                
                btnEditAC.setVisible(true);
                btnEditStaff.setVisible(true);
            }
        });
        jpAcContent.add(jspAC);
        
        //-----------------form, 2nd column of this borderlayout ^^
        JPanel jpAcForm = new JPanel(new GridBagLayout());
        jpAcContent.add(jpAcForm);
        GridBagConstraints c2 = new GridBagConstraints();
        c2.gridx = 0;
        c2.gridy = 0;
        c2.weightx = 1;
        c2.weighty = 1;
        c2.anchor = GridBagConstraints.LINE_END;
        //AC form 1st column
        c2.ipadx = 100;
        jpAcForm.add(new JLabel("Due:"), c2);
        c2.gridy++;
        c2.anchor = GridBagConstraints.LINE_START;
        jpAcForm.add(new JLabel("STAFF"), c2);
        c2.gridy++;
        c2.anchor = GridBagConstraints.LINE_END;
        jpAcForm.add(new JLabel("ID:"), c2);
        c2.gridy++;
        jpAcForm.add(new JLabel("Name:"), c2);
        c2.gridy++;
        jpAcForm.add(new JLabel("Car Allocation:"), c2);
        c2.gridy+=2;
        c2.anchor = GridBagConstraints.LINE_START;
        jpAcForm.add(new JLabel("CAR DETAILS"), c2);
        c2.anchor = GridBagConstraints.LINE_END;
        c2.gridy++;
        jpAcForm.add(new JLabel("Make:"), c2);
        c2.gridy++;
        jpAcForm.add(new JLabel("Model:"), c2);
        c2.gridy++;
        jpAcForm.add(new JLabel("Year:"), c2);
        
        
        //AC form 2nd colum
        c2.gridx++;
        c2.gridy = 0;
        c2.anchor = GridBagConstraints.LINE_START;
        lblDueAC = new JLabel("");
        jpAcForm.add(lblDueAC, c2);
        c2.gridy+=2;
        lblIdAC = new JLabel("");
        jpAcForm.add(lblIdAC, c2);
        c2.gridy++;
        lblNameAC = new JLabel("");
        jpAcForm.add(lblNameAC, c2);
        c2.gridy++;
        lblCarAllocationAC = new JLabel("");
        jpAcForm.add(lblCarAllocationAC, c2);
        c2.gridy++;
        btnEditStaff = new JButton("Edit");
        btnEditStaff.setVisible(false);
        btnEditStaff.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EditStaff editStaffDialog = new EditStaff(null, true);
                editStaffDialog.setStaff(staffCurrent);
                editStaffDialog.setVisible(true);
            }
        });
        jpAcForm.add(btnEditStaff, c2);
        c2.gridy+=2;
        lblMakeAC = new JLabel("");
        jpAcForm.add(lblMakeAC, c2);
        c2.gridy++;
        lblModelAC = new JLabel("");
        jpAcForm.add(lblModelAC, c2);
        c2.gridy++;
        lblYearAC = new JLabel("");
        jpAcForm.add(lblYearAC, c2);
        c2.gridy++;
        btnEditAC = new JButton("Edit");
        btnEditAC.setVisible(false);
        btnEditAC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EditCar editCarDialog = new EditCar(null, true);
                editCarDialog.setCar(carCurrent);
                editCarDialog.setVisible(true);
            }
        });
        jpAcForm.add(btnEditAC, c2);
        
        //</editor-fold>
        
        //Create Car Allocation (CCA)
        //<editor-fold defaultstate="collapsed" desc="Create Car Allocation Initialisation">
        JPanel jpCcaCont = new JPanel(new BorderLayout());
        container.add(jpCcaCont, "create panel");
        
        
        //CCA header
        JPanel jpCcaHeaderCont = new JPanel(new GridLayout(0,1));
        jpCcaCont.add(jpCcaHeaderCont, BorderLayout.NORTH);
        
        JPanel jpCcaHeader1 = new JPanel(new BorderLayout());
        jpCcaHeader1.setBorder(new EmptyBorder(10, 10, 10, 10));
        jpCcaHeaderCont.add(jpCcaHeader1);
        JButton btnCcaBack = new JButton("< Back");
        btnCcaBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.first(container);
            }
        });
        jpCcaHeader1.add(btnCcaBack, BorderLayout.WEST);
        
        JPanel jpCcaHeader2 = new JPanel(new BorderLayout());
        jpCcaHeaderCont.add(jpCcaHeader2);
        jpCcaHeader2.setBorder(new EmptyBorder(0, 30, 0, 0));
        JLabel lblCaCars = new JLabel("Select a car:");
        jpCcaHeader2.add(lblCaCars, BorderLayout.WEST);
        
        //CCA body content
        JPanel jpCcaBody = new JPanel(new GridLayout(1, 2));
        jpCcaCont.add(jpCcaBody, BorderLayout.CENTER);
        
        //----------------CCA body content 1st column. CAR LIST 
        JScrollPane jspCca = new JScrollPane();
        jspCca.setBorder(new EmptyBorder(0, 20, 20, 0));
        //jspCca.setViewportView(lstCarType);
        jspCca.getViewport().add(lstCarType);
        jpCcaBody.add(jspCca);
        
        lstCarType.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt){
                int index = lstCarType.getSelectedIndex();
                if(cars != null){
                    if(index != -1){ //prevent from throwing arrayindexoutofbounds
                        
                        //carCurrent = cars.get(index);
                        carCurrent = (Car)lstCarType.getModel().getElementAt(index);
                        lblMakeCCA.setText(carCurrent.getMake());
                        lblModelCCA.setText(carCurrent.getModel());
                        lblYearCCA.setText(String.valueOf(carCurrent.getYear()));
                        lblRegistrationCCA.setText(carCurrent.getRegisterNumber());
                        btnSubmit.setVisible(true);
                    }
                }
            }
        });
        
        
        //----------------CCA body content 2nd column. FORM
        JPanel jpCcaForm = new JPanel(new GridBagLayout());
        jpCcaBody.add(jpCcaForm);
        GridBagConstraints c3 = new GridBagConstraints();
        c3.gridx = 0;
        c3.gridy = 0;
        c3.weightx = 1;
        c3.weighty = 1;
        c3.anchor = GridBagConstraints.LINE_START;
        c3.ipadx = 100;
        
        //form 1st col
        jpCcaForm.add(new JLabel("STAFF"), c3);
        c3.gridy++;
        c3.anchor = GridBagConstraints.LINE_END;
        jpCcaForm.add(new JLabel("ID:"), c3);
        c3.gridy++;
        jpCcaForm.add(new JLabel("Name:"), c3);
        c3.gridy++;
        jpCcaForm.add(new JLabel("Date of Birth"), c3);
        c3.gridy++;
        jpCcaForm.add(new JLabel("Car Type:"), c3);
        c3.gridy++;
        c3.anchor = GridBagConstraints.LINE_START;
        jpCcaForm.add(new JLabel("CAR DETAILS"), c3);
        c3.anchor = GridBagConstraints.LINE_END;
        c3.gridy++;
        jpCcaForm.add(new JLabel("Registration No.:"), c3);
        c3.gridy++;
        jpCcaForm.add(new JLabel("Make:"), c3);
        c3.gridy++;
        jpCcaForm.add(new JLabel("Model:"), c3);
        c3.gridy++;
        jpCcaForm.add(new JLabel("Year:"), c3);
        c3.gridy++;
        jpCcaForm.add(new JLabel("Loan Type:"), c3);
        
        //form 2nd col
        c3.gridy = 1;
        c3.gridx = 1;
        c3.ipadx = 70;
        c3.anchor = GridBagConstraints.LINE_START;
        
        JTextField txtSearchId = new JTextField();
        txtSearchId.setToolTipText("Enter Staff ID");
        jpCcaForm.add(txtSearchId, c3);
        c3.gridy++;
        lblNameCCA = new JLabel("");
        jpCcaForm.add(lblNameCCA, c3);
        c3.gridy++;
        lblDobCCA = new JLabel("");
        jpCcaForm.add(lblDobCCA, c3);
        c3.gridy++;
        lblCarTypeCCA = new JLabel("");
        jpCcaForm.add(lblCarTypeCCA, c3);
        c3.gridy+=2;
        lblRegistrationCCA = new JLabel("");
        jpCcaForm.add(lblRegistrationCCA, c3);
        c3.gridy++;
        lblMakeCCA = new JLabel("");
        jpCcaForm.add(lblMakeCCA, c3);
        c3.gridy++;
        lblModelCCA = new JLabel("");
        jpCcaForm.add(lblModelCCA, c3);
        c3.gridy++;
        lblYearCCA = new JLabel("");
        jpCcaForm.add(lblYearCCA, c3);
        c3.gridy++;
        cbxLoan = new JComboBox();
        String[] loanTypes = {"Short Term", "Long Term"};
        modelLoan = new DefaultComboBoxModel<>(loanTypes);
        cbxLoan.setModel(modelLoan);
        jpCcaForm.add(cbxLoan, c3);
        c3.gridy++;
        c3.ipadx = 0;
        c3.anchor = GridBagConstraints.LINE_START;
        btnSubmit = new JButton("Submit");
        btnSubmit.setVisible(false);
        
        btnSubmit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CarAllocation cA = new CarAllocation();
                cA.setStaff(staffCurrent);
                cA.setCar(carCurrent);
                Duration dur;
                if(cbxLoan.getSelectedItem().equals("Short Term")){
                    dur = Duration.ofDays(1);
                }
                else
                {
                    dur = Duration.ofDays(365);
                    
                }
                cA.setDuration(dur);
                cA.setDtAllocated(LocalDateTime.now(ZoneId.systemDefault()));
                carCurrent.setState(CarState.ONLOAN);
                carAllocList.addCarAllocation(cA);
                
                refreshList();
                resetLabels();
                
                
                JOptionPane.showMessageDialog(null, "Submitted!");
            }
        });
        jpCcaForm.add(btnSubmit, c3);
        
        //form 3rd col
        c3.gridx = 2;
        c3.gridy = 1;
        c3.anchor = GridBagConstraints.LINE_START;
        
        JButton btnFindStaff = new JButton("Find");
        btnFindStaff.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String id = txtSearchId.getText();
                refreshList();
                resetLabels();
                if(findStaff(id)){
                    lblNameCCA.setText(staffCurrent.getName());
                    lblDobCCA.setText(staffCurrent.getDob().toString());
                    lblCarTypeCCA.setText(staffCurrent.getCarType().toString());
                }
//                else
//                    resetLabels();
                
            }
        });
        jpCcaForm.add(btnFindStaff, c3);
        //</editor-fold>
        
        //Staffs
        //<editor-fold defaultstate="collapsed">
        JPanel jpStaffs = new JPanel(new BorderLayout());
        
        JPanel staffTitle = new JPanel(new FlowLayout(FlowLayout.LEFT, 100, 0));
        staffTitle.setBorder(new EmptyBorder(10,10,10,10));
        
        JButton btnStaffBack  = new JButton("< Back");
        btnStaffBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cardLayout.show(container, "main menu");
            }
        });
        staffTitle.add(btnStaffBack);
        staffTitle.add(new JLabel("Staffs"));
        jpStaffs.add(staffTitle, BorderLayout.NORTH);
        
        JPanel staffBody = new JPanel(new GridLayout(1, 2));
        jpStaffs.add(staffBody, BorderLayout.CENTER);
        
        JScrollPane jspStaffs = new JScrollPane(staffList);
        jspStaffs.setBorder(new EmptyBorder(0,20,20,20));
        staffBody.add(jspStaffs);
        
        JPanel jpStaffGrid = new JPanel(new GridBagLayout());
        GridBagConstraints c4 = new GridBagConstraints();
        
        staffBody.add(jpStaffGrid);
        
        btnCreateStaff = new JButton("new staff");
        btnCreateStaff.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CreateStaff createStaffDialog = new CreateStaff(null, true);
                createStaffDialog.setParent(UniSystem.this);
                createStaffDialog.setVisible(true);
            }
        });
        
        c4.gridx = 0;
        c4.gridy = 0;
        //c4.gridwidth = 2;
        c4.insets = new Insets(20,20,20,20);
        c4.anchor = GridBagConstraints.FIRST_LINE_END;
        jpStaffGrid.add(btnCreateStaff, c4);
        c4.gridx++;
        
        JButton btnStaffEdit = new JButton("Edit");
        btnStaffEdit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                EditStaff editStaffDialog = new EditStaff(null, true);
                editStaffDialog.setParent(UniSystem.this);
                editStaffDialog.setStaff(staffCurrent);
                editStaffDialog.setVisible(true);
            }
        });
        jpStaffGrid.add(btnStaffEdit, c4);
        
        c4.gridx = 0;
        c4.gridy++;
        c4.gridwidth = 1;
        c4.ipadx = 20;
        c4.ipady = 20;
        c4.anchor = GridBagConstraints.LINE_END;
        c4.ipadx = 70;
        
        jpStaffGrid.add(new JLabel("ID:"), c4);
        c4.gridy++;
        jpStaffGrid.add(new JLabel("Name:"), c4);
        c4.gridy++;
        jpStaffGrid.add(new JLabel("Gender:"), c4);
        c4.gridy++;
        jpStaffGrid.add(new JLabel("DOB:"), c4);
        c4.gridy++;
        jpStaffGrid.add(new JLabel("Pay Grade:"), c4);
        c4.gridy++;
        jpStaffGrid.add(new JLabel("Car Allocation:"), c4);
        
        JLabel lblStaffID = new JLabel();
        JLabel lblStaffName = new JLabel();
        JLabel lblStaffDOB = new JLabel();
        JLabel lblStaffGender = new JLabel();
        JLabel lblStaffGrade = new JLabel();
        JLabel lblStaffCar = new JLabel();
        
        c4.gridy = 1;
        c4.gridx = 1;
        c4.anchor = GridBagConstraints.CENTER;
        
        jpStaffGrid.add(lblStaffID, c4);
        c4.gridy++;
        jpStaffGrid.add(lblStaffName, c4);
        c4.gridy++;
        jpStaffGrid.add(lblStaffDOB, c4);
        c4.gridy++;
        jpStaffGrid.add(lblStaffGender, c4);
        c4.gridy++;
        jpStaffGrid.add(lblStaffGrade, c4);
        c4.gridy++;
        jpStaffGrid.add(lblStaffCar, c4);
        c4.gridy++;
        
        staffList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                
                int index = staffList.getSelectedIndex();
                staffCurrent = staffs.get(index);
                lblStaffID.setText(staffCurrent.getStaffId());
                lblStaffName.setText(staffCurrent.getName());
                lblStaffDOB.setText(staffCurrent.getDob().toString());
                lblStaffGender.setText(staffCurrent.getGender());
                lblStaffGrade.setText(staffCurrent.getGrade().toString());
                lblStaffCar.setText(staffCurrent.getCarType().toString());
            }
            
        });
        
        container.add(jpStaffs, "staffs");
        
        
        //</editor-fold>
        
        //Menu Bar
        menuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenuItem miOpen = new JMenuItem("Open");
        JMenuItem miSave = new JMenuItem("Save");
        miSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save();
            }
        });
        miOpen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                load();
            }
        });
        file.add(miOpen);
        file.add(miSave);
        menuBar.add(file);
        
        setJMenuBar(menuBar);
        //-------------------------------------------------------------------
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(900, 720);
        setResizable(false);
        setVisible(true);
        //-------------------------------------------------------------------
    }
    
    private void load() {
        
        File objFile = new File("data.txt");
            try (ObjectInputStream objIn = new ObjectInputStream(
                            new BufferedInputStream(
                            new FileInputStream(objFile))))
            {
                Object obj = objIn.readObject();
                UniSystemWrapper tempWrapper = (UniSystemWrapper)obj;
                
                //de-register as an observer
                for(Car car : cars) {
                    car.removeObserver(this);
                }
                
                for(Staff staff : staffs) {
                    staff.removeObserver(this);
                }
                
               
                carAllocList = tempWrapper.getCarAllocList();
                for(Car car : tempWrapper.getCars()) {
                    addCar(car);
                }
                
                for(Car car : tempWrapper.getAllocatedCars()) {
                    allocatedCars.add(car);
                }
                
                for(Staff staff : tempWrapper.getStaffs()) {
                    addStaff(staff);
                }
                
                JOptionPane.showMessageDialog(this, "Loaded successfully.");
            } catch (IOException ex)
            {
                JOptionPane.showMessageDialog(this, "Error loading data.");
            }
            catch(ClassNotFoundException cex)
            {
                JOptionPane.showMessageDialog(this, "Error reading data!!!");
            }
        
        JOptionPane.showMessageDialog(null, "Method not implemented");
    }
    
    private void save(){
        
        File objFile = new File("data.txt");
            try (ObjectOutputStream objOut = new ObjectOutputStream(
                            new BufferedOutputStream(
                            new FileOutputStream(objFile))))
            {
                wrapper.setAllocatedCars(allocatedCars);
                wrapper.setCarAllocList(carAllocList);
                wrapper.setCars(cars);
                wrapper.setStaffs(staffs);
                
                objOut.writeObject(wrapper);
                JOptionPane.showMessageDialog(this,
                        "Saved.");
            } catch (IOException ex)
            {
                JOptionPane.showMessageDialog(this,
                        "Error saving data model.");
            }
        
        JOptionPane.showMessageDialog(null, "Method not implemented");
    }
    
    private void resetLabels(){
        
        lblMake.setText("");
        lblModel.setText(""); 
        lblYear.setText(""); 
        lblRegistration.setText(""); 
        lblDoors.setText(""); 
        lblNextService.setText("");
        lblDueAC.setText("");
        lblIdAC.setText("");
        lblNameAC.setText("");
        lblCarAllocationAC.setText("");
        lblMakeAC.setText("");
        lblModelAC.setText("");
        lblYearAC.setText("");
        lblNameCCA.setText("");
        lblDobCCA.setText("");
        lblCarTypeCCA.setText("");
        lblRegistrationCCA.setText("");
        lblMakeCCA.setText("");
        lblModelCCA.setText("");
        lblYearCCA.setText("");
    }
    
    private void refreshList(){
    
        System.out.println("Refresh list method");
        modelAllCars.clear();
        modelAllocatedCars.clear();
        modelCarType.clear();
        modelServices.clear();
        modelAllocations.clear();
        modelStaffs.clear();
        
        
        if(!staffs.isEmpty()) {
            for(Staff staff : staffs) {
                modelStaffs.addElement(staff.getStaffId() + " - " + staff.getName());
            }
        }
        
        if(carCurrent != null){
            for(LocalDate date : carCurrent.getServiceDates()){
                modelServices.addElement(date.toString());
            }
        }
        
        if(!carAllocList.isEmpty()){
            for(CarAllocation x : carAllocList) {
                //modelAllocations.addElement(x.getStaff().getName() + " " + x.getCar().getRegisterNumber());
                modelAllocations.addElement(x);
            }
        }
        
        if(cars != null){
            for(Car car : cars){
            modelAllCars.addElement(car.getMake() + " " + car.getModel() + " " + car.getYear());
            }
        
            for(Car car:cars){
                if(car.getState() == CarState.ONLOAN){
                    modelAllocatedCars.addElement(car.getMake() + " " + car.getModel() + " " + car.getRegisterNumber());
                    allocatedCars.add(car);
                }
            }
            
            if(staffCurrent != null){
                //modelCarType
                switch(staffCurrent.getCarType()){
                    case ANYCAR:
                        for(Car car:cars){
                            if(car.getState() == CarState.AVAILABLE){
                                //modelCarType.addElement(car.getMake() + " " + car.getModel() + " " + car.getYear());
                                modelCarType.addElement(car);
                            }
                        }
                        break;
                    case SALOONANDLUXURY:
                        for(Car car:cars){
                            if(car.getState() == CarState.AVAILABLE && (car.getClassification().equals(CarTypeAllocation.SALOON) || car.getClassification().equals(CarTypeAllocation.SALOONANDLUXURY))){
                                //modelCarType.addElement(car.getMake() + " " + car.getModel() + " " + car.getYear());
                                modelCarType.addElement(car);
                            }
                        }
                        break;
                    case SALOON:
                        for(Car car:cars){
                            if(car.getState() == CarState.AVAILABLE && car.getClassification().equals(CarTypeAllocation.SALOON)){
                                //modelCarType.addElement(car.getMake() + " " + car.getModel() + " " + car.getYear());
                                modelCarType.addElement(car);
                            }
                        }
                }
            }
        }
        
    }
    
    public void addCar(Car carObj){
        
        if(carObj != null){
            carObj.registerObserver(this);
            cars.add(carObj);
            refreshList();
        }
    }
    
    public void addStaff(Staff staff){
        if(staff != null){
            staff.registerObserver(this);
            staffs.add(staff);
            refreshList();
        }
    }
    
    private Boolean findStaff(String id){
        boolean result = false;
        
        for(Staff tStaff:staffs){
            if(tStaff.getStaffId().equals(id)){
                staffCurrent = tStaff;
                result = true;
                refreshList();
            }
        }
        
        return result;
    }

    @Override
    public void update() {
        refreshList();
    }
    
}
