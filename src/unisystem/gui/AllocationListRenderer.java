/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unisystem.gui;

import java.awt.Color;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import unisystem.model.CarAllocation;

/**
 *
 * @author Marc Gil Sadumiano
 */
public class AllocationListRenderer extends DefaultListCellRenderer implements ListCellRenderer<Object>{

    @Override
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        
        CarAllocation allocation = (CarAllocation)value;
        setText(allocation.getStaff().getName() + " - " + allocation.getCar().getRegisterNumber());
        setToolTipText(allocation.getDtAllocated().toString());
        setBackground(Color.white);
        return this;
    }
    
}
